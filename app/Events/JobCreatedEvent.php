<?php

namespace App\Events;

/**
 * JobCreatedEvent is event to be fired 
 * when job is created
 *
 * @author ramy
 */
class JobCreatedEvent {
    
    /**
     * created Job
     */
    
    public $job;
    
    
    public function __construct($job) {
        $this->job=$job;
    }
    
    
}
