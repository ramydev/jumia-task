<?php

namespace App\Contracts;

Interface JobsRepositoryInterface {
    
    public function find($id);
    public function findAll();
    public function create(array $array);
    public function remove($id);
}