<?php
namespace App\Repositories;

use App\Contracts\JobsRepositoryInterface;
use App\Models\Jobs;

class JobsRepository implements JobsRepositoryInterface
{
    private $model;
    
    /**
     * 
     * @param Jobs $model
     */
    public function __construct(Jobs $model){
        $this->model=$model;
    }
    
    /**
     * Find one job
     * @param type $id
     * @return type
     */
    public function find($id) {
        return $this->model->select('*')->where('id',$id)->get();
    }
    
    /**
     * Find all jobs
     * @return type
     */
    public function findAll() {
        return $this->model->select('*')->get();
    }

    /**
     * Remove job
     * @param type $jobID
     * @return type
     */
    public function remove($jobID) {
        return $this->model->where('id',$jobID)->delete();
    }

    /**
     * Save new job
     * @param array $data
     */
    public function create(array $data) {
       return $this->model->insert($data);
    }


}

