<?php

namespace App\Http\Controllers;

use App\Services\JobsService;
use App\traits\ApiResponse;
use Illuminate\Http\Request;

class JobsController extends Controller 
{
    use ApiResponse;
    
    private $jobsService;
    
    /**
     * Create a new JobsController instance.
     *
     * @return void
     */
    public function __construct(JobsService $jobsService)
    {
        $this->jobsService=$jobsService;
    }
    
    /**
     * Get All Jobs
     */
    public function all() { 
        try{
            $jobs = $this->jobsService->getAll();
           return $this->response(200, $jobs);
        } catch (\Exception $exception){
           return $this->response(200, [],$exception->getMessage());
        }
        

    }
    
    /**
     * Register new job
     * @param \App\Http\Controllers\Request $job
     */
    public function register(Request $job) {
        try{
            $job=$this->jobsService->registerNewJob($job->only('title','description'));
           return $this->response(200, $job,'New Job created Successfully');
        } catch (\Exception $exception){
            return $this->response(200, [],$exception->getMessage());
        }
    }
  
}
