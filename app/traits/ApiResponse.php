<?php

namespace App\traits;

/**
 * Description of ApiResponse
 *
 * @author ramy
 */
trait ApiResponse {
   
    public function response($code,$data,$message=''){
         return response()->json(['data'=>$data,'message'=>$message],$code);
    }
}
