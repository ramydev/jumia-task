<?php
 
namespace App\Services;
use App\Repositories\JobsRepository;

/**
 * JobsService
 *
 * @author ramy
 */
class JobsService {
    private $jobsRepo;
    
    public function __construct(JobsRepository $jobsRepo) {
        $this->jobsRepo=$jobsRepo;
    }
    
    /**
     * Get All jobs
     * @return type
     */
    public function getAll() {
        return $this->jobsRepo->findAll();
    }
    
    /**
     * Create new job
     * @param type $jobArray
     * @return type
     */
    public function registerNewJob($jobArray) {
        return $this->jobsRepo->create($jobArray);
    }
}
