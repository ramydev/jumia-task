<?php


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of JobsTest
 *
 * @author ramy
 */
class JobsTest extends TestCase{

    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_all_jobs()
    {
        $response = $this->json('GET','/jobs/all');
        $response->seeJsonStructure([
            'message',
            'data' => [
                    [ 
                      'id',
                      'title',
                      'description',
                      'created_at',
                      'updated_at'
                   ] 
                ]
        ]);
        $response->assertResponseStatus(200);
    }
    
    public function test_register_job(){
        $response = $this->json('POST','/jobs/register', [
            'title'=>'PHP developer',
            'description'=>'Some description about job'
        ])->seeJson(['data'=>true]);
        
       $this->assertResponseStatus(200);
    }

}
